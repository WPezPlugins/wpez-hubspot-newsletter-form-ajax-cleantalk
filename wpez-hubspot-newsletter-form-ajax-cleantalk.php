<?php
/**
 * Plugin Name: WPezPlugins: Hubspot Newsletter Form AJAX > Cleantalk
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-hubspot-newsletter-form-ajax-cleantalk
 * Description: Uses the WPexHNFA spamCheck filter with the https://CleanTalk.org/ API to mitigate spam submitted to HS
 * Version: 0.0.0
 * Author: MF Simchock (Chief Executive Alchemist) at Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez-hnfact
 *
 * @package WPezHNFACT
 */

namespace WPezHNFACT;

use Cleantalk\Antispam\CleantalkRequest as CleantalkRequest;
use Cleantalk\Antispam\Cleantalk as Cleantalk;


// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

// https://gitlab.com/WPezPlugins/wpez-hubspot-newsletter-form-ajax/-/blob/main/App/Src/ClassHubspotNewsletterFormAjax.php#L214
\add_filter( 'WPezHNFA\App\Src\wpAjaxCallback\spamCheck', __NAMESPACE__ . '\spamCheck', 10,2 );

/**
 * Callback for the filter: WPezHNFA\App\Src\wpAjaxCallback\spamCheck .
 *
 * @param array $arr_args Assoc array with keys: 'active' and 'is_spam'. This array - or array with the same keys - gets returned.
 * @param array $arr_hs_fields The fields + values submitted via the form
 * 
 * @return array
 */
function spamCheck( $arr_args, array $arr_hs_fields = array() ) {

	// If the classes aren't there...load our own within the plugin?
	if ( ! class_exists( 'Cleantalk\Antispam\CleantalkRequest' ) || ! class_exists( 'Cleantalk\Antispam\Cleantalk' ) ) {

		// Cleantalk plugin is MIA.
		return $arr_args;
	}

	// No email? which should never happen, but it doesn't hurt to make sure. 
	if ( ! isset( $arr_hs_fields['email'] ) || empty( $arr_hs_fields['email'] ) ) {

		return $arr_args;
	}

	// Use the option from the primary CleanTalk plugin. No need to reinvent that wheel.
	$cleantalk_settings = get_option( 'cleantalk_settings' );
	if ( ! isset( $cleantalk_settings['apikey'] ) || empty( $cleantalk_settings['apikey'] ) ) {

		// No Cleantalk API key
		return $arr_args;
	}

	$cleantalk_apikey = $cleantalk_settings['apikey'];

	$sender_ip = null;
	if ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
		$sender_ip = $_SERVER['REMOTE_ADDR'];
	}
	
	$arr_hs_fields_defaults = array(
		'firstname' => '',
		'lastname'  => '',
	);
	$arr_hs_fields = array_merge( $arr_hs_fields_defaults, $arr_hs_fields );

	$ct_request = new CleantalkRequest();

	ray( $ct_request );

	$ct_request->auth_key     = $cleantalk_apikey; // $auth_key;
	$ct_request->agent        = 'php-api';
	$ct_request->sender_email = $arr_hs_fields['email'];
	$ct_request->sender_ip    = $sender_ip;
	// If we have name field(s) pass them along to CT, else don't.
	if ( ! empty( $arr_hs_fields['firstname'] ) || ! empty( $arr_hs_fields['lastname'] ) ) {
		$ct_request->sender_nickname = $arr_hs_fields['firstname'] . ' ' . $arr_hs_fields['lastname'];
	}
	// TODO - what is this JS setting?
	$js_on = 0;
	$ct_request->js_on       = $js_on;
	$ct_request->submit_time = time() - (int) $_SESSION['ct_submit_time'];

	$ct             = new Cleantalk();
	$config_url     = 'http://moderate.cleantalk.org/api2.0/';
	$ct->server_url = $config_url;

	// Check
	$ct_result = $ct->isAllowUser( $ct_request );

	// ! inactive === active
	if ( 1 !== $ct_result->inactive ) {
		$arr_args['active'] = true; // = spam check off
	}

	// ! allowed === spam!!!
	if ( 1 !== $ct_result->allow ) {
		// echo 'User allowed. Reason ' . $ct_result->comment;
		// ray( 'User allowed. Reason ' . $ct_result->comment );
		$arr_args['is_spam'] = true;
	}
	return $arr_args;

}