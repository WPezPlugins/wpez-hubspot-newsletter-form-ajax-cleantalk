## WPezPlugins: Hubspot Newsletter Form AJAX > Cleantalk

__Adds CleanTalk (https://CleanTalk.org) spam checking - No CAPTCHA! - to WPezPlugins: Hubspot Newsletter Form AJAX (https://gitlab.com/WPezPlugins/wpez-hubspot-newsletter-form-ajax) via a filter in that plugin.__


### OVERVIEW

Note: This plugin gets the CleanTalk auth / creds, as well as used a couple of classes from the CleanTalk's WordPress plug (link in References section) so you'll need to have that plugin installed and activated. 

AFAIK, you're also going to need a CleanTalk subscription. Their prices are very reasonable considering  your users won't be subjected to CAPTCHA. It's not free but as they say, "You get what you pay for."


### REFERENCES

- https://wordpress.org/plugins/cleantalk-spam-protect/

- https://cleantalk.org/my/bill/recharge


### TODO

- Once the POC / beta stage is over, refactor (into something that reflect the ez coding aesthetic).


### CHANGE LOG

__-- 0.0.0 - Fri 26 Nov 2021__

- INIT - Hey! Ho!! Let's go!!!